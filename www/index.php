<?php

use Acclimate\Container\ContainerAcclimator;
use Bazo\Rest\App;
use Bazo\Rest\InvalidPathException;
use Bazo\Rest\RestException;
use Bazo\Rest\Router;
use REST\NetteContainerCallbackResolver;
use Symfony\Component\HttpFoundation\JsonResponse;
use Tracy\Debugger;

$configurator	 = require __DIR__ . '/../app/bootstrap.php';
$container		 = $configurator->createContainer();

$router = $container->getByType(Router::class);

$onException = function (Exception $ex, $code, $payload) use ($container) {

	if ($container->parameters['debugMode']) {
		throw $ex;
	}

	if (!($ex instanceof RestException)) {
		Debugger::log($ex);
	}

	$response = new JsonResponse($payload, $code);
	$response->send();
	exit;
};

try {
	$app = new App;

	$acclimator	 = new ContainerAcclimator;
	$acclimated	 = $acclimator->acclimate($container);

	$resolver = new NetteContainerCallbackResolver($container);

	$router->setCallbackResolver($resolver);

	$app->attach($router);

	$app->onException[] = function($e) {
		throw $e;
	};

	$app->run();
} catch (InvalidPathException $ex) {
	$code	 = 404;
	$payload = [
		'error'		 => $code,
		'message'	 => 'Not found. ' . $ex->getMessage()
	];
	$onException($ex, $code, $payload);
} catch (RestException $ex) {
	$code	 = $ex->getCode();
	$payload = [
		'error'		 => $code,
		'message'	 => $ex->getMessage()
	];
	$onException($ex, $code, $payload);
} catch (Exception $ex) {
	$code	 = 500;
	$payload = [
		'error'		 => $code,
		'message'	 => get_class($ex) . ':' . $ex->getMessage()
	];
	$onException($ex, $code, $payload);
}
