<?php

class MySqlRepositoryException extends RuntimeException {}
class MySqlWatchNotFoundException extends MySqlRepositoryException {}
class XmlLoaderException extends RuntimeException {}

class EntityNotFoundException extends RuntimeException{}
class WatchNotFoundException extends EntityNotFoundException {}
