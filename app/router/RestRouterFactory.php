<?php

use Bazo\Rest\Methods;
use Bazo\Rest\Patterns;
use Bazo\Rest\Route;
use Bazo\Rest\Router;
use Controllers\WatchController;

/**
 * Router factory.
 */
class RestRouterFactory
{

	/**
	 * REST Routing
	 * @return Router
	 */
	public function createRESTRouter()
	{
		$router = new Router;

		// i would use plural watches as per recommended rest api practices
		$router[] = new Route(['/watch/{id}', [
				'id' => Patterns::PATTERN_NUM,
			]], [
			Methods::GET	 => [WatchController::class, 'get'],
		]);

		return $router;
	}


}
