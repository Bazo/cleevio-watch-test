<?php

/**
 * @author Martin Bažík <martin@bazo.sk>
 */
class MySqlWatchDTO
{

	/** @var int */
	public $id;
	
	/** @var string */
	public $title;
	
	/** @var int */
	public $price;
	
	/** @var string */
	public $description;

	public function __construct($id, $title, $price, $description)
	{
		$this->id			 = $id;
		$this->title		 = $title;
		$this->price		 = $price;
		$this->description	 = $description;
	}



}
