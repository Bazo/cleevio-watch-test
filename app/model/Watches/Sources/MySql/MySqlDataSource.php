<?php

namespace Watches\Sources\MySql;

use Watches\Sources\IWatchDataSource;


/**
 * @author Martin Bažík <martin@bazo.sk>
 */
class MySqlDataSource implements IWatchDataSource
{

	/** @var array */
	private $watches;

	/** @var MySqlWatchRepository */
	private $repository;

	/**
	 * @param type $watches
	 * @param MySqlWatchRepository $repository
	 */
	public function __construct($watches/* , MySqlWatchRepository $repository */)
	{
		$this->watches = $watches;
		//$this->repository	 = $repository;
	}


	/**
	 * @param int $id
	 * @return array
	 * @throws \WatchNotFoundException
	 */
	public function load(int $id): array
	{
		try {
			//fake using the repository to load data
			//$watchDto = $this->repository->getWatchById($id);
			if (!isset($this->watches[$id])) {
				throw new \MySqlWatchNotFoundException;
			}
			$watchRaw = $this->watches[$id];

			$watchDto = \MySqlWatchDTO($watchRaw['identification'], $watchRaw['title'], $watchRaw['price'], $watchRaw['description']);
		} catch (\MySqlWatchNotFoundException $e) {
			throw new \WatchNotFoundException('Watch not found', $e->getCode(), $e);
		}
		return (array) $watchDto;
	}


}
