<?php

namespace Watches\Sources\Xml;

use Watches\Sources\IWatchDataSource;


/**
 * @author Martin Bažík <martin@bazo.sk>
 */
class XmlDataSource implements IWatchDataSource
{

	/** @var array */
	private $watches;

	/** @var XmlWatchLoader */
	private $loader;

	/**
	 * @param type $watches
	 * @param XmlWatchLoader $loader
	 */
	public function __construct($watches/* , XmlWatchLoader $loader */)
	{
		$this->watches = $watches;
		//$this->loader	 = $loader;
	}


	/**
	 * @param int $id
	 * @return array
	 * @throws \WatchNotFoundException
	 */
	public function load(int $id): array
	{
		try {
			//fake using the loader to load data
			//return $this->loader->loadByIdFromXml($id);

			if (!isset($this->watches[$id])) {
				throw new \XmlLoaderException;
			}

			return $this->watches[$id];
		} catch (XmlLoaderException $e) {
			throw new \WatchNotFoundException('Watch not found', $e->getCode(), $e);
		}
	}


}
