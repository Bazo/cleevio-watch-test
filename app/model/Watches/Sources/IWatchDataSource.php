<?php

namespace Watches\Sources;


/**
 * @author Martin Bažík <martin@bazo.sk>
 */
interface IWatchDataSource
{

	/**
	 * @param int $id
	 */
	public function load(int $id): array;
}
