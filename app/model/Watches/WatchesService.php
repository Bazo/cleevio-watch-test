<?php

namespace Watches;


use Nette\Caching\Cache;
use Nette\Caching\IStorage;
use Watches\Sources\IWatchDataSource;

/**
 * @author Martin Bažík <martin@bazo.sk>
 */
class WatchesService
{

	/** @var IStorage */
	private $cacheStorage;

	/** @var IWatchDataSource */
	private $dataSource;

	/** @var Cache */
	private $cache;

	/**
	 * @param IStorage $cacheStorage
	 * @param \Watches\IWatchDataSource $dataSource
	 */
	public function __construct(IStorage $cacheStorage, IWatchDataSource $dataSource)
	{
		$this->cacheStorage	 = $cacheStorage;
		$this->dataSource	 = $dataSource;

		$this->cache = new Cache($cacheStorage, 'watches');
	}


	/**
	 * @param type $id
	 * @return array
	 */
	public function load($id): array
	{
		return $this->cache->load($id, function() use($id) {
					return $this->dataSource->load($id);
				});
	}


}
