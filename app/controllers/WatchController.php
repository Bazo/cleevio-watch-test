<?php

namespace Controllers;


use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * @author Martin Bažík <martin@bazo.sk>
 */
class WatchController extends Controller
{

	/** @var \Watches\WatchesService */
	private $watchesService;

	public function __construct(\Watches\WatchesService $watchesService)
	{
		$this->watchesService = $watchesService;
	}


	public function get(Request $req, Response $res): void
	{
		$id = (int) $req->get('id');

		$res->setStatusCode(Response::HTTP_OK);

		$watch = $this->getByIdAction($id);

		if (is_null($watch)) {
			$error = [
				'error' => sprintf('Watch with id %d not found', $id)
			];
			$this->respond($res, Response::HTTP_NOT_FOUND, $error);
		}

		$res->setSharedMaxAge(31536000);
		$res->setExpires(new \DateTime('+1 year'));
		$this->respond($res, Response::HTTP_OK, $watch);
	}

	public function getByIdAction($id): ?array
	{
		try {
			return $this->watchesService->load($id);
		} catch (\WatchNotFoundException $e) {
			return NULL;
		}
	}


}
