<?php

namespace Controllers;


use Symfony\Component\HttpFoundation\Response;

/**
 * @author Martin Bažík <martin@bazo.sk>
 */
abstract class Controller
{

	protected function respond(Response $res, $code = Response::HTTP_OK, $payload = NULL)
	{
		$res->setStatusCode($code);
		if (!is_null($payload)) {
			$res->headers->set('Cache-Control', 's-maxage=0, max-age=0, must-revalidate');
			$res->headers->set('Expires', 'Mon, 23 Jan 1978 10:00:00 GMT');
			$res->headers->set('Content-Type', 'application/json');
			$res->setContent(json_encode($payload));
		}

		$res->send();
		exit;
	}


	protected function error(Response $res, $code = Response::HTTP_BAD_REQUEST, $errors = '')
	{
		if (!\is_array($errors)) {
			$errors = [$errors];
		}

		$message = [
			'errors' => $errors
		];

		$this->respond($res, $code, $message);
	}


}
